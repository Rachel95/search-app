package com.assignment.searchapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Collections;
import java.util.List;

@RestController
public class SearchEngineController {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${search.service.url}")
    private String searchServiceUrl;

    @Value("${ad.service.url}")
    private String adServiceUrl;

    @CrossOrigin
    @GetMapping
    public ResponseEntity<SearchResult> search(@RequestParam("query") String searchTerm) {
        return ResponseEntity.ok(process(searchTerm));
    }

    private SearchResult process(final String searchTerm) {
        SearchResult result = new SearchResult();

        result.setWebpages(getWebPages(searchTerm));
        result.setAds(getAds(searchTerm));

        return result;
    }

    private List<WebPageResource> getWebPages(final String searchTerm) {
        ResponseEntity<List<WebPageResource>> webPageResponse =
                restTemplate.exchange(searchServiceUrl + String.format("?query=%s", searchTerm),
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<WebPageResource>>() {});

        return webPageResponse.getBody() != null ? webPageResponse.getBody() : Collections.emptyList();
    }


    private List<AdPageResource> getAds(final String searchTerm) {
        ResponseEntity<List<AdPageResource>> adPageResponse =
                restTemplate.exchange(adServiceUrl + String.format("?query=%s", searchTerm),
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<AdPageResource>>() {});

        return adPageResponse.getBody() != null ? adPageResponse.getBody() : Collections.emptyList();
    }

}
