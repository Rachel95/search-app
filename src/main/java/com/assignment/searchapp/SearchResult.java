package com.assignment.searchapp;

import java.util.List;

public class SearchResult {

    List<WebPageResource> webpages;

    List<AdPageResource> ads;

    public List<WebPageResource> getWebpages() {
        return webpages;
    }

    public void setWebpages(List<WebPageResource> webpages) {
        this.webpages = webpages;
    }

    public List<AdPageResource> getAds() {
        return ads;
    }

    public void setAds(List<AdPageResource> ads) {
        this.ads = ads;
    }
}
